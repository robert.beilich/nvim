local M = {}

M.toggle = function(set)
  if set ~= nil then
    M.opts.show = set
  else
    M.opts.show = not M.opts.show
  end

	if M.opts.show then
		vim.diagnostic.show()
	else
		vim.diagnostic.hide()
	end
end

M.opts = {
  show = true,
  diagnostics = {
    virtual_text = { prefix = require("sebaran.config").icons.diagnostics.prefix },
    severity_sort = true,
  },
}

M.setup = function()
	for name, icon in pairs(require("sebaran.config").icons.diagnostics.signs) do
		name = "DiagnosticSign" .. name:sub(1, 1):upper() .. name:sub(2)
		vim.fn.sign_define(name, { text = icon, texthl = name, numhl = "" })
	end
	vim.diagnostic.config(M.opts.diagnostics)

  M.toggle(M.opts.show)
end

return M
