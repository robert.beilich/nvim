inoremap("jk", "<esc>")

map({ "n", "v" }, "j", "gj")
map({ "n", "v" }, "k", "gk")

vnoremap("<tab>", ">gv", { silent = true })
vnoremap("<s-tab>", "<gv", { silent = true })
vnoremap(">", ">gv", { silent = true })
vnoremap("<", "<gv", { silent = true })

nnoremap("<leader>do", vim.diagnostic.open_float, { desc = "Line Diagnostics" })
nnoremap("<leader>dt", require("sebaran.diagnostics").toggle, { desc = "Toggle Diagnostics" })

-- TODO: find replacement
-- nnoremap("<space>", "za")

nnoremap("ZX", 'ggVG"+yZQ', { desc = "Copy all and quit (without saving)" })
