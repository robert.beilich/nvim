vim.g.mapleader = " "
vim.g.maplocalleader = " "

vim.opt.shortmess:append("Ic")
vim.o.updatetime = 50

vim.o.tabstop = 2
vim.o.softtabstop = 2
vim.o.shiftwidth = 2
vim.o.expandtab = true

vim.o.scrolloff = 10

vim.o.ignorecase = true
vim.o.smartcase = true

vim.o.inccommand = "split"

vim.o.number = true
vim.o.relativenumber = true

vim.o.undofile = true

vim.o.termguicolors = true

vim.o.splitright = true

vim.g.tex_flavor = "latex"
