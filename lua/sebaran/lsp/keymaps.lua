local M = {}

M._keys = nil

M.get = function()
	local format = require("sebaran.lsp.format").format

	if not M._keys then
		M._keys = {
			{ "<leader>rn", vim.lsp.buf.rename, desc = "Rename", has = "rename" },
			{ "<leader>qf", vim.lsp.buf.code_action, mode = { "n", "v" }, desc = "Code Action", has = "codeAction" },
			{ "<leader>p", format, desc = "Format Document", has = "documentFormatting" },
			{ "<leader>p", format, mode = "v", desc = "Format Range", has = "documentRangeFormatting" },
			{ "gd", require("telescope.builtin").lsp_definitions, desc = "Goto Defintion", has = "definition" },
			{ "gD", vim.lsp.buf.declaration, desc = "Goto Declaration" },
			{ "gr", require("telescope.builtin").lsp_references, desc = "Goto References" },
			{ "gI", require("telescope.builtin").lsp_implementations, desc = "Goto Impelementations" },

			{ "K", vim.lsp.buf.hover, desc = "Hover" },
			{ "gK", vim.lsp.buf.signature_help, desc = "Signature Help", has = "signatureHelp" },

			{ "]d", M.diagnostic_goto("next"), desc = "Next Diagnostic" },
			{ "[d", M.diagnostic_goto("prev"), desc = "Prev Diagnostic" },
			{ "]e", M.diagnostic_goto("next", "ERROR"), desc = "Next Error" },
			{ "[e", M.diagnostic_goto("prev", "ERROR"), desc = "Prev Error" },
			{ "]w", M.diagnostic_goto("next", "WARN"), desc = "Next Warning" },
			{ "[w", M.diagnostic_goto("prev", "WARN"), desc = "Prev Warning" },
		}
	end

  return require("sebaran.lazy.util").keymaps(M._keys)
end

M.diagnostic_goto = function(direction, severity)
	local go = vim.diagnostic["goto_" .. direction]
	severity = severity and vim.diagnostic.severity[severity] or nil
	return function()
		go({ severity = severity })
	end
end

M.on_attach = function(client, buffer)
	local Keys = require("lazy.core.handler.keys")

	for _, keys in pairs(M.get()) do
		if not keys.has or client.server_capabilities[keys.has .. "Provider"] then
			local opts = Keys.opts(keys)

			opts.has = nil
			opts.silent = opts.silent ~= false
			opts.buffer = buffer
			map(keys.mode or "n", keys[1], keys[2], opts)
		end
	end
end

return M
