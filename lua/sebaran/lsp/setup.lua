local M = {}

M._attach_hooks = {
	require("sebaran.lsp.format").on_attach,
	require("sebaran.lsp.keymaps").on_attach,
}

M.add_attach_hook = function(hook)
	M._attach_hooks[#M._attach_hooks + 1] = hook
end

M.setup = function(opts)
	require("sebaran.lsp.format").autoformat = opts.autoformat

	for _, hook in pairs(M._attach_hooks) do
		M._on_attach(hook)
	end

	local mlsp = require("mason-lspconfig")
	local all_mslp_servers = vim.tbl_keys(require("mason-lspconfig.mappings.server").lspconfig_to_package)

	local servers = opts.servers
	local capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

	local function manual_setup(server)
		local server_opts = vim.tbl_deep_extend("force", {
			capabilities = vim.deepcopy(capabilities),
		}, servers[server] or {})

		if opts.setup[server] then
			if opts.setup[server](server, server_opts) then
				return
			end
		elseif opts.setup["*"] then
			if opts.setup["*"](server, server_opts) then
				return
			end
		end
		require("lspconfig")[server].setup(server_opts)
	end

	local servers = opts.servers
	local ensure_installed = {}
	for server, server_opts in pairs(servers) do
		if server_opts then
			server_opts = server_opts == true and {} or server_opts
			if server_opts.mason == false or not vim.tbl_contains(all_mslp_servers, server) then
				manual_setup(server)
			else
				ensure_installed[#ensure_installed + 1] = server
			end
		end
	end

	mlsp.setup({ ensure_installed = ensure_installed })
	mlsp.setup_handlers({ manual_setup })
end

M._on_attach = function(on_attach)
	local util = require("lspconfig.util")
	util.on_setup = util.add_hook_after(util.on_setup, function(config)
		if config.on_attach then
			config.on_attach = util.add_hook_after(config.on_attach, on_attach)
		else
			config.on_attach = on_attach
		end
	end)
end

return M
