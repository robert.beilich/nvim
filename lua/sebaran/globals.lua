local keymap = require("theprimeagen/keymap")
---@diagnostic disable: lowercase-global
map = keymap.map
imap = keymap.imap
nnoremap = keymap.nnoremap
inoremap = keymap.inoremap
vnoremap = keymap.vnoremap
snoremap = keymap.snoremap
---@diagnostic enable: lowercase-global

P = function(v)
	print(vim.inspect(v))
	return v
end
