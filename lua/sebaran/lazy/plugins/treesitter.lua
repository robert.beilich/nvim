return {
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		keys = {
			{ "<c-space>", desc = "Increment selection" },
			{ "<bs>", desc = "Decrement selection", mode = "x" },
		},
		opts = {
			ensure_installed = "all",
			sync_install = false,
			highlight = {
				enable = true,
				additional_vim_regex_highlighting = false,
			},
			incremental_selection = {
				enable = true,
				keymaps = {
					init_selection = "<c-space>",
					node_incremental = "<c-space>",
					node_decremental = "<bs>",
				},
			},
			indent = {
				enable = true,
			},
			context_commentstring = { enable = true, enable_autocmd = false },
		},
		config = function(_, opts)
			require("nvim-treesitter.configs").setup(opts)
		end,
	},
	{
		"nvim-treesitter/playground",
		cmd = "TSPlaygroundToggle",
		dependencies = {
			"nvim-treesitter/nvim-treesitter",
			opts = {
				playground = {
					enable = true,
				},
			},
		},
	},
	{
		"nvim-treesitter/nvim-treesitter-textobjects",
		dependencies = {
			"nvim-treesitter/nvim-treesitter",
			opts = {
				textobjects = {
					select = {
						enable = true,
						lookahead = true,
						keymaps = {
							["af"] = { query = "@function.outer", desc = "Select around function" },
							["if"] = { query = "@function.inner", desc = "Select in function" },
							["ac"] = { query = "@class.outer", desc = "Select around class" },
							["ic"] = { query = "@class.inner", desc = "Select in class" },
						},
					},
					swap = {
						enable = true,
						swap_next = {
							["<leader>>"] = "@parameter.inner",
						},
						swap_previous = {
							["<leader><"] = "@parameter.inner",
						},
					},
				},
			},
		},
	},
}
