return {
	{
		"vimwiki/vimwiki",
		keys = {
			{ "<leader>ww", desc = "Open Vimwiki" },
		},
		config = function()
			vim.g.vimwiki_valid_html_tags = "b,i,s,u,sub,sup,kbd,br,hr,span,div"
		end,
	},
	{
		"folke/zen-mode.nvim",
		keys = {
			{ "<Leader>G", ":ZenMode<cr>", silent = true, desc = "Toggle ZenMode" },
		},
		opts = {
			window = {
				backdrop = 0.95,
				width = 80,
				height = 0.85,
				options = {
					signcolumn = "no",
					number = false,
					relativenumber = false,
				},
			},
			plugins = {
				options = {
					enabled = true,
					ruler = false,
					showcmd = false,
				},
				gitsigns = { enabled = true },
				tmux = { enabled = true },
			},
		},
	},
	{
		"folke/twilight.nvim",
		opts = {
			dimming = {
				inactive = true,
			},
		},
		keys = {
			{ "<Leader>l", ":Twilight<cr>", silent = true, desc = "Toggle Twilight" },
		},
	},
}
