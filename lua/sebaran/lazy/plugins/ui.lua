return {
	{
		"catppuccin/nvim",
		name = "catppuccin",
		priority = 1000,
		opts = {
			integrations = {
				dap = {
					enabled = true,
					enable_ui = true,
				},
				dim_inactive = {
					percentage = 0.8,
				},
				native_lsp = {
					enabled = true,
					virtual_text = {
						errors = { "italic" },
						hints = { "bold" },
						warnings = { "bold" },
						information = { "bold" },
					},
					underlines = {
						errors = { "underline" },
						hints = { "underline" },
						warnings = { "underline" },
						information = { "underline" },
					},
				},
			},
		},
		config = function(plug, opts)
			vim.g.catppuccin_flavour = "mocha"
			require(plug.name).setup(opts)
			vim.cmd.colorscheme(plug.name)
		end,
	},
	{
		"nvim-lualine/lualine.nvim",
		opts = {
			sections = {
				lualine_b = {
					"branch",
					{
						"diff",
						symbols = require("sebaran.config").icons.git,
					},
				},
				lualine_c = {
					{
						"filename",
						separator = "|",
					},
					{
						function()
							return require("nvim-navic").get_location()
						end,
						cond = function()
							return package.loaded["nvim-navic"] and require("nvim-navic").is_available()
						end,
					},
				},
				lualine_x = {
					{ "diagnostics", symbols = require("sebaran.config").icons.diagnostics.signs },
				},
				lualine_y = {
					{ "encoding", separator = "" },
					{ "fileformat", separator = "" },
					{ "filetype", icon_only = true, separator = "" },
				},
				lualine_z = {
					{ "progress", separator = "", padding = { left = 1, right = 1 } },
					{ "location", padding = { left = 0, right = 1 } },
				},
			},
			inactive_sections = {
				lualine_c = {
					{
						"filename",
						path = 1,
					},
				},
			},
		},
	},
	{
		"lukas-reineke/indent-blankline.nvim",
		event = { "BufReadPost", "BufNewFile" },
		keys = {
			{
				"<leader>til",
				function()
					vim.g.indent_blankline_enabled = not vim.g.indent_blankline_enabled
				end,
				desc = "Toggle indent lines",
			},
		},
		opts = {
			enabled = false,
			char = "│",
			filetype_exclude = { "help", "lazy" },
			show_trailing_blankline_indent = false,
			show_current_context = true,
		},
	},
	{
		"stevearc/dressing.nvim",
		lazy = true,
		opts = {
			input = {
				override = function(conf)
					conf.col = -1
					conf.row = 0
					return conf
				end,
			},
		},
		init = function()
			---@diagnostic disable-next-line: duplicate-set-field
			vim.ui.select = function(...)
				require("lazy").load({ plugins = { "dressing.nvim" } })
				return vim.ui.select(...)
			end
			---@diagnostic disable-next-line: duplicate-set-field
			vim.ui.input = function(...)
				require("lazy").load({ plugins = { "dressing.nvim" } })
				return vim.ui.input(...)
			end
		end,
	},
	{
		"rcarriga/nvim-notify",
		keys = {
			{
				"<leader>un",
				function()
					require("notify").dismiss({ silent = true, pending = true })
				end,
				desc = "Delete all Notifications",
			},
		},
		event = { "VeryLazy" },
		opts = {
			timeout = 3000,
			max_height = function()
				return math.floor(vim.o.lines * 0.75)
			end,
			max_width = function()
				return math.floor(vim.o.columns * 0.75)
			end,
		},
		config = function()
			vim.notify = require("notify")
		end,
	},
	{ "nvim-tree/nvim-web-devicons", lazy = true },
}
