return {
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"mason.nvim",
			"williamboman/mason-lspconfig.nvim",
			"hrsh7th/cmp-nvim-lsp",
			{ "folke/neoconf.nvim", cmd = "Neoconf", config = true },
		},
		event = { "BufReadPre", "BufNewFile" },
		opts = {
			autoformat = true,
			servers = {},
			setup = {},
		},
		config = function(_, opts)
			require("sebaran.lsp.setup").setup(opts)
		end,
	},
	{
		"williamboman/mason.nvim",
		cmd = "Mason",
		opts = {
			ensure_installed = {
				"stylua",
				"shfmt",
			},
		},
		config = function(_, opts)
			require("mason").setup(opts)
			local mr = require("mason-registry")
			local function ensure_installed()
				for _, tool in ipairs(opts.ensure_installed) do
					local p = mr.get_package(tool)
					if not p:is_installed() then
						p:install()
					end
				end
			end
			if mr.refresh then
				mr.refresh(ensure_installed)
			else
				ensure_installed()
			end
		end,
	},
	{
		"jose-elias-alvarez/null-ls.nvim",
		event = { "BufReadPre", "BufNewFile" },
		dependencies = { "mason.nvim" },
		opts = function(_, opts)
			local null_ls = require("null-ls")
			return require("lazy.core.util").merge(opts, {
				root_dir = require("null-ls.utils").root_pattern(".null-ls-root", ".neoconf.json", "Makefile", ".git"),
				sources = {
					null_ls.builtins.formatting.stylua,
					null_ls.builtins.formatting.shfmt,
				},
			})
		end,
	},
	{ "ray-x/lsp_signature.nvim" },
	{
		"SmiteshP/nvim-navic",
		dependencies = { "neovim/nvim-lspconfig" },
		lazy = true,
		opts = {
			icons = require("sebaran.config").icons.kinds,
			highlight = true,
			depth_limit = 5,
		},
		init = function()
			vim.g.navic_silence = true
			require("sebaran.lsp.setup").add_attach_hook(function(client, buffer)
				if client.server_capabilities.documentSymbolProvider then
					require("nvim-navic").attach(client, buffer)
				end
			end)
		end,
	},
}
