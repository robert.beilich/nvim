return {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			if type(opts.ensure_installed) == "table" then
				vim.list_extend(opts.ensure_installed, { "lua" })
			end
		end,
	},
	{
		"neovim/nvim-lspconfig",
		opts = function(_, opts)
			return require("lazy.core.util").merge(opts, {
				servers = {
					lua_ls = {
						settings = {
							Lua = {
								workspace = {
									checkThirdParty = false,
								},
								completion = {
									callSnippet = "Replace",
								},
							},
						},
					},
				},
			})
		end,
	},
}
