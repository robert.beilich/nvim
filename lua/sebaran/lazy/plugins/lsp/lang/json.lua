return {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			if type(opts.ensure_installed) == "table" then
				vim.list_extend(opts.ensure_installed, { "json", "json5", "jsonc" })
			end
		end,
	},
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"b0o/SchemaStore.nvim",
		},
		opts = function(_, opts)
			return require("lazy.core.util").merge(opts, {
				servers = {
					jsonls = {
						settings = {
							json = {
								format = {
									enable = true,
								},
								schemas = require("schemastore").json.schemas(),
								validate = { enable = true },
							},
						},
					},
					yamlls = {
						settings = {
							yaml = {
								hover = true,
								completion = true,
								schemas = require("schemastore").json.schemas(),
								validate = true,
							},
						},
					},
				},
			})
		end,
	},
}
