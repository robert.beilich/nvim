return {
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"folke/neodev.nvim",
			opts = {
				pathStrict = true,
			},
		},
	},
}
