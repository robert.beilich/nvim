return {
	{
		"TimUntersberger/neogit",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"sindrets/diffview.nvim",
		},
		keys = {
			{
				"<leader>gs",
				function()
					require("neogit").open()
				end,
				desc = "Git Status",
			},
		},
		opts = {
			kind = "split_above",
			integrations = {
				diffview = true,
			},
		},
	},
	{ "rhysd/committia.vim" },
	{
		"lewis6991/gitsigns.nvim",
		event = { "BufReadPre", "BufNewFile" },
		opts = {
			current_line_blame_opts = {
				delay = 0,
			},
			signs = require("sebaran.config").icons.gitsigns,
			on_attach = function(bufnr)
				local gs = package.loaded.gitsigns

				nnoremap("<leader>gb", gs.toggle_current_line_blame, { buffer = bufnr, desc = "Blame Line" })
				nnoremap("<leader>gd", gs.diffthis, { desc = "Diff" })
				nnoremap("<leader>ghp", gs.preview_hunk, { desc = "Preview Hunk" })
				nnoremap("<leader>hs", gs.stage_hunk, { desc = "Stage Hunk" })
				nnoremap("<leader>hr", gs.reset_hunk, { desc = "Reset Hunk" })
				vnoremap("<leader>hs", function()
					gs.stage_hunk({ vim.fn.line("."), vim.fn.line("v") })
				end, { desc = "Stage Hunk" })
				vnoremap("<leader>hr", function()
					gs.reset_hunk({ vim.fn.line("."), vim.fn.line("v") })
				end, { desc = "Reset Hunk" })
				map({ "o", "x" }, "ih", ":<C-U>Gitsigns select_hunk<CR>", { desc = "Select Hunk" })
			end,
		},
	},
}
