local function telescope(fun, opts)
	return function()
		require("telescope.builtin")[fun](opts)
	end
end

return {
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
		cmd = "Telescope",
		keys = {
			{ "<leader>fr", telescope("resume"), desc = "Resume Find" },

			{ "<leader><leader>", telescope("find_files"), desc = "Files" },
			{ "<leader>:", telescope("command_history"), desc = "Comand History" },

			{ "<leader>ff", telescope("find_files"), desc = "Files" },
			{ "<leader>fb", telescope("buffer"), desc = "Buffers" },
			{ "<leader>fh", telescope("help_tags"), desc = "Help" },
			{ "<leader>fd", telescope("diagnostics"), desc = "Diagnostics" },

			{ "<leader>sg", telescope("live_grep"), desc = "Grep" },
      { "<leader>sk", telescope("keymaps"), desc = "Keymaps" },

      { "<leader>uC", telescope("colorscheme", { enable_preview = true }), desc = "Colorscheme" }
		},
		config = function()
			-- lazy load telescope-fzf-native
			require("telescope").load_extension("fzf")
		end,
	},
	{
		"nvim-telescope/telescope-fzf-native.nvim",
		dependencies = { "nvim-telescope/telescope.nvim" },
		build = "make",
		lazy = true,
	},
	{
		"nvim-telescope/telescope-project.nvim",
		dependencies = { "nvim-telescope/telescope.nvim" },
		keys = {
			{
				"<leader>fp",
				function()
					require("telescope").extensions.project.project()
				end,
        desc = "Find Project"
			},
		},
		config = function()
			require("telescope").load_extension("project")
		end,
	},
}
