return {
	{ "tpope/vim-surround" },
	{
		"windwp/nvim-autopairs",
		opts = {
			check_ts = true,
			ts_config = {
				lua = { "string" },
				javascript = { "template_string" },
			},
			fast_wrap = {},
		},
	},
	{
		"windwp/nvim-ts-autotag",
		dependencies = {
			"nvim-treesitter/nvim-treesitter",
			opts = {
				autotag = {
					enable = true,
				},
			},
		},
	},
	{ "wellle/targets.vim" },

	{
		"Wansmer/treesj",
		keys = {
			{
				"gJ",
				function()
					require("treesj").join()
				end,
				desc = "Join Node",
			},
			{
				"gS",
				function()
					require("treesj").split()
				end,
				desc = "Split Node",
			},
		},
		dependencies = { "nvim-treesitter/nvim-treesitter" },
		opts = {
			use_default_keymaps = false,
		},
	},

	{
		"JoosepAlviste/nvim-ts-context-commentstring",
		dependencies = {
			"nvim-treesitter/nvim-treesitter",
			opts = {
				context_commentstring = {
					enable = true,
					enable_autocmd = false,
				},
			},
		},
	},
	{
		"numToStr/Comment.nvim",
		opts = function(_, opts)
			return require("lazy.core.util").merge(opts, {
				pre_hook = require("ts_context_commentstring.integrations.comment_nvim").create_pre_hook(),
			})
		end,
	},
	{
		"folke/todo-comments.nvim",
		cmd = { "TodoTelescope" },
		event = { "BufReadPost", "BufNewFile" },
		keys = {
			{
				"]t",
				function()
					require("todo-comments").jump_next()
				end,
				desc = "Next Todo",
			},
			{
				"[t",
				function()
					require("todo-comments").jump_prev()
				end,
				desc = "Prev Todo",
			},
			{ "<leader>ft", "<cmd>TodoTelescope<cr>", desc = "Todos" },
		},
		config = true,
	},
}
