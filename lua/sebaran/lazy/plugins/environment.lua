return {
	{ "tpope/vim-repeat", event = "VeryLazy" },
	{ "farmergreg/vim-lastplace" },
	{ "airblade/vim-rooter" },
	{ "gioele/vim-autoswap" },
	{ "tpope/vim-eunuch" },

	{ "pgdouyon/vim-evanesco" },
	{ "jeffkreeftmeijer/vim-numbertoggle" },

	{
		"szw/vim-maximizer",
		config = function()
			vim.g.maximizer_restore_on_winleave = 1
		end,
	},
	{
		"wesQ3/vim-windowswap",
		keys = {
			{ "<leader>sw", "<cmd>call WindowSwap#EasyWindowSwap()<cr>", silent = true, desc = "Swap Windows" },
		},
		config = function()
			vim.g.windowswap_map_keys = 0
		end,
	},
}
