require("sebaran/lazy/bootstrap")

require("lazy").setup({
	spec = {
		{ import = "sebaran/lazy/plugins" },
		{ import = "sebaran/lazy/plugins/lsp/lang" },
	},
})
