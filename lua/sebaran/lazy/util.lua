local M = {}

function M.on_very_lazy(fn)
	vim.api.nvim_create_autocmd("User", {
		pattern = "VeryLazy",
		callback = function()
			fn()
		end,
	})
end

function M.values(plugin_name, prop)
	local plugin = require("lazy.core.config").plugins[plugin_name]
	if not plugin then
		return {}
	end
	local Plugin = require("lazy.core.plugin")
	return Plugin.values(plugin, prop, false)
end

function M.opts(plugin)
	return M.values(plugin, "opts")
end

function M.keys(plugin)
	return M.values(plugin, "keys")
end

function M.keymaps(mappings)
	local Keys = require("lazy.core.handler.keys")

	local keymaps = {}
	for _, value in ipairs(mappings) do
		local keys = Keys.parse(value)
		if keys[2] == vim.NIL or keys[2] == false then
			keymaps[keys.id] = nil
		else
			keymaps[keys.id] = keys
		end
	end

	return keymaps
end

function M.setup_keys(plugin, mappings)
	local Keys = require("lazy.core.handler.keys")

	local keymaps = require("lazy.core.util").merge(M.keymaps(M.keys(plugin)), M.keymaps(mappings))

	for _, keys in pairs(keymaps) do
		local opts = Keys.opts(keys)
		map(keys.mode or "n", keys[1], keys[2], opts)
	end
end

return M
